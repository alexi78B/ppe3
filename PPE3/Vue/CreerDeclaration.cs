﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PPE3.Container;
using MySql.Data.MySqlClient;


namespace PPE3.Vue
{
    public partial class CreerDeclaration : Form
    {
        MySqlConnection connection = new MySqlConnection("Database=mydb_abes;Data Source=srv-mydon.sio.local;User Id=abes;Password=22/12/1998");
        private string _idDocker;

        public CreerDeclaration(string codeDocker)
        {
            InitializeComponent();
            _idDocker = codeDocker;
        }

        /// <summary>
        /// Initialise les valeurs dans les champs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreerDeclaration_Load(object sender, EventArgs e)
        {
            Traite.Items.Add("Oui");
            Traite.Items.Add("Non");

            Urgent.Items.Add("Oui");
            Urgent.Items.Add("Non");

            Probleme.Items.Add("Corrosion");
            Probleme.Items.Add("Choc sur container");
            Probleme.Items.Add("Paroi percée");
            Probleme.Items.Add("Problème de fermeture");
            Probleme.Items.Add("Autre");

            NumContainer.DataSource = Containers.AfficheNumContainer();

            string numContainer = NumContainer.Text;
            string commentaire = Commentaire.Text;
        }

        /// <summary>
        /// Récupère le contenu des champs pour créer une déclaration
        /// </summary>
        public void AjoutDeclaration()
        {
            int numeroContainer = 0;
            string description, probleme, traite, urgent;
            numeroContainer = Convert.ToInt32(NumContainer.SelectedItem.ToString());
            description = Commentaire.Text;
            probleme = Probleme.Text;
            traite = Traite.SelectedItem.ToString();
            urgent = Urgent.SelectedItem.ToString();

            Declaration laDeclaration = new Declaration()
            {
                NumContainer = numeroContainer,
                CommentaireDeclaration = description,
                CodeProbleme = probleme,
                DateDeclaration = DateTime.Today,
                Urgence = urgent,
                Traite = traite,
                CodeDocker = _idDocker
            };
            laDeclaration.Insert();
        }

        private void Valider_Click(object sender, EventArgs e)
        {
            AjoutDeclaration();
            MenuDeclaration menuDeclaration = new MenuDeclaration(_idDocker);
            menuDeclaration.Show();
            this.Hide();
        }

        private void Retour_Click(object sender, EventArgs e)
        {
            MenuDeclaration menuDeclaration = new MenuDeclaration(_idDocker);
            menuDeclaration.Show();
            this.Hide();
        }
    }
}
