﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using PPE3.Container;

namespace PPE3.Vue
{
    public partial class Consultation : Form
    {
        //private static string connectionString = "Database=mydb_abes;Data Source=srv-mydon.sio.local;User Id=abes;Password=22/12/1998"; // connexion Mydon
        private static string connectionString = "Database=mydb_abes;Data Source=sts.ferry-conflans.net;User Id=abes;Password=22/12/1998"; // connexion Mydon
        public static MySqlConnection GetConnexion
        {
            get
            {
                return new MySqlConnection(connectionString);
            }

        }
        public Consultation()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            MenuDeclaration menuDeclaration = new MenuDeclaration();
            menuDeclaration.Show();
            this.Hide();
        }

        private void Consultation_Load(object sender, EventArgs e)
        {
            MySqlDataAdapter da = new MySqlDataAdapter("Select * from DECLARATION", GetConnexion);
            DataTable dt = new DataTable();
            da.Fill(dt);

            for(int i = 0; i< dt.Rows.Count;i++)
            {
                DataRow dr = dt.Rows[i];
                ListViewItem itm = new ListViewItem(dr["codeDeclaration"].ToString());
                itm.SubItems.Add(dr["commentaireDeclaration"].ToString());
                itm.SubItems.Add(dr["dateDeclaration"].ToString());
                itm.SubItems.Add(dr["urgence"].ToString());
                itm.SubItems.Add(dr["traite"].ToString());
                itm.SubItems.Add(dr["probleme"].ToString());
                itm.SubItems.Add(dr["numContainer"].ToString());
                listView1.Items.Add(itm);
            }
        }

        private void ListView1_DoubleClick(object sender, EventArgs e)
        {
            ModifSuprDeclaration modifSuprDeclaration = new ModifSuprDeclaration();
            modifSuprDeclaration.Show();
            this.Hide();
        }
    }
}
