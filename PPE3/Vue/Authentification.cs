﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.DirectoryServices;

namespace PPE3.Vue
{
    public partial class Authentification : Form
    {
        public Authentification()
        {
            InitializeComponent();
        }

        private void Valider_Click(object sender, EventArgs e)
        {
            try
            {
                DirectoryEntry Ldap = new DirectoryEntry("LDAP://sio.local/ OU=OU-SISR,OU=OU-Etudiants,DC=sio,DC=local", @"abes", "22/12/1998");
                //DirectoryEntry Ldap = new DirectoryEntry("LDAP://172.16.0.120/ OU=OU-SLAM,OU=OU-Etudiants,DC=sio,DC=local", @"abes", "22/12/1998");
                //DirectoryEntry Ldap = new DirectoryEntry("LDAP://sio.local/ OU=OU-SLAM,OU=OU-Etudiants,DC=sio,DC=local", textBox1.Text, textBox2.Text);
                DirectoryEntry de = Ldap.NativeObject as DirectoryEntry;
                string _idDocker = textBox1.Text;
                MenuDeclaration menuDeclaration = new MenuDeclaration(_idDocker);
                menuDeclaration.Show();
                this.Hide();
            }

            catch (Exception Ex)

            {
                MessageBox.Show("erreur LDAP " + Ex.Message);
            }
        }
    }
}
