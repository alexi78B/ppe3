﻿namespace PPE3.Vue
{
    partial class CreerDeclaration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Commentaire = new System.Windows.Forms.TextBox();
            this.Probleme = new System.Windows.Forms.ComboBox();
            this.Urgent = new System.Windows.Forms.ComboBox();
            this.Traite = new System.Windows.Forms.ComboBox();
            this.Valider = new System.Windows.Forms.Button();
            this.NumContainer = new System.Windows.Forms.ComboBox();
            this.Retour = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(546, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Déclaration d\'un problème";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(537, 143);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(149, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Commentaire de la déclaration";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(119, 420);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Problème";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(119, 347);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Numéro de container";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(830, 347);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Urgent";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(830, 415);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Tarité";
            // 
            // Commentaire
            // 
            this.Commentaire.Location = new System.Drawing.Point(400, 169);
            this.Commentaire.Multiline = true;
            this.Commentaire.Name = "Commentaire";
            this.Commentaire.Size = new System.Drawing.Size(422, 118);
            this.Commentaire.TabIndex = 8;
            // 
            // Probleme
            // 
            this.Probleme.FormattingEnabled = true;
            this.Probleme.Location = new System.Drawing.Point(252, 412);
            this.Probleme.Name = "Probleme";
            this.Probleme.Size = new System.Drawing.Size(200, 21);
            this.Probleme.TabIndex = 12;
            // 
            // Urgent
            // 
            this.Urgent.FormattingEnabled = true;
            this.Urgent.Location = new System.Drawing.Point(987, 339);
            this.Urgent.Name = "Urgent";
            this.Urgent.Size = new System.Drawing.Size(200, 21);
            this.Urgent.TabIndex = 13;
            // 
            // Traite
            // 
            this.Traite.FormattingEnabled = true;
            this.Traite.Location = new System.Drawing.Point(987, 407);
            this.Traite.Name = "Traite";
            this.Traite.Size = new System.Drawing.Size(200, 21);
            this.Traite.TabIndex = 14;
            // 
            // Valider
            // 
            this.Valider.Location = new System.Drawing.Point(400, 580);
            this.Valider.Name = "Valider";
            this.Valider.Size = new System.Drawing.Size(137, 65);
            this.Valider.TabIndex = 15;
            this.Valider.Text = "Valider";
            this.Valider.UseVisualStyleBackColor = true;
            this.Valider.Click += new System.EventHandler(this.Valider_Click);
            // 
            // NumContainer
            // 
            this.NumContainer.FormattingEnabled = true;
            this.NumContainer.Location = new System.Drawing.Point(252, 344);
            this.NumContainer.Name = "NumContainer";
            this.NumContainer.Size = new System.Drawing.Size(200, 21);
            this.NumContainer.TabIndex = 16;
            // 
            // Retour
            // 
            this.Retour.Location = new System.Drawing.Point(685, 580);
            this.Retour.Name = "Retour";
            this.Retour.Size = new System.Drawing.Size(137, 65);
            this.Retour.TabIndex = 17;
            this.Retour.Text = "Retour";
            this.Retour.UseVisualStyleBackColor = true;
            this.Retour.Click += new System.EventHandler(this.Retour_Click);
            // 
            // CreerDeclaration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1249, 741);
            this.Controls.Add(this.Retour);
            this.Controls.Add(this.NumContainer);
            this.Controls.Add(this.Valider);
            this.Controls.Add(this.Traite);
            this.Controls.Add(this.Urgent);
            this.Controls.Add(this.Probleme);
            this.Controls.Add(this.Commentaire);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "CreerDeclaration";
            this.Text = "CreerDeclaration";
            this.Load += new System.EventHandler(this.CreerDeclaration_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox Commentaire;
        private System.Windows.Forms.ComboBox Probleme;
        private System.Windows.Forms.ComboBox Urgent;
        private System.Windows.Forms.ComboBox Traite;
        private System.Windows.Forms.Button Valider;
        private System.Windows.Forms.ComboBox NumContainer;
        private System.Windows.Forms.Button Retour;
    }
}