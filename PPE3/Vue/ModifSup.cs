﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PPE3.Container;
using MySql.Data.MySqlClient;

namespace PPE3.Vue
{
    public partial class ModifSup : Form
    {
        private Declaration _declaration;
        private string _idDocker;

        public ModifSup(Declaration uneDeclaration, string codeDocker)
        {
            InitializeComponent();
            _declaration = uneDeclaration;
            _idDocker = codeDocker;

        }


        /// <summary>
        /// Charge les éléments de la déclaration et propose les autres 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ModifSup_Load(object sender, EventArgs e)
        {
            Traite.Items.Add("Oui");
            Traite.Items.Add("Non");

            Urgent.Items.Add("Oui");
            Urgent.Items.Add("Non");

            Probleme.Items.Add("Corrosion");
            Probleme.Items.Add("Choc sur container");
            Probleme.Items.Add("Paroi percée");
            Probleme.Items.Add("Problème de fermeture");
            Probleme.Items.Add("Autre");

            Traite.Text = _declaration.Traite;
            Urgent.Text = _declaration.Urgence;
            Probleme.Text = _declaration.CodeProbleme;
            Commentaire.Text = _declaration.CommentaireDeclaration;
            CodeDeclaration.Text = _declaration.CodeDeclaration.ToString();
            NumContainer.Text = _declaration.NumContainer.ToString();
        }

        /// <summary>
        /// Sauvegarde la déclaration avec ces modifications
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Sauvegarder_Click(object sender, EventArgs e)
        {
            _declaration.Traite = Traite.Text;
            _declaration.Urgence = Urgent.Text;
            _declaration.CodeProbleme = Probleme.Text;
            _declaration.CommentaireDeclaration = Commentaire.Text;
            _declaration.Sauvegarder();
            ConsultationDeclaration consultation = new ConsultationDeclaration(_idDocker);
            consultation.Show();
            this.Hide();
        }

        /// <summary>
        /// Supprime la déclaration
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Supprimer_Click(object sender, EventArgs e)
        {
            _declaration.Supprimer();
            ConsultationDeclaration consultation = new ConsultationDeclaration(_idDocker);
            consultation.Show();
            this.Hide();
        }

        private void RetourMenu_Click(object sender, EventArgs e)
        {
            MenuDeclaration menuDeclaration = new MenuDeclaration(_idDocker);
            menuDeclaration.Show();
            this.Hide();
        }
    }
}
