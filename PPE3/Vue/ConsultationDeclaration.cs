﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PPE3.Container;
using MySql.Data.MySqlClient;

namespace PPE3.Vue
{
    public partial class ConsultationDeclaration : Form
    {
        private string _idDcoker;
        public ConsultationDeclaration(string codeDocker)
        {
            InitializeComponent();
            _idDcoker = codeDocker;
        }
        /// <summary>
        /// Retourne au menu précédent
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Retour_Click(object sender, EventArgs e)
        {
            MenuDeclaration menuDeclaration = new MenuDeclaration(_idDcoker);
            menuDeclaration.Show();
            this.Hide();
        }

        /// <summary>
        /// Affiche les déclarations d'un docker
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConsultationDeclaration_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = Declaration.FetchAll(_idDcoker);
        }

        /// <summary>
        /// Récupère les données de la ligne souhaitée
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                Declaration declaration = dataGridView1.SelectedRows[0].DataBoundItem as Declaration;
                ModifSup modifSup = new ModifSup(declaration, _idDcoker);
                modifSup.Show();
                this.Hide();
            }
        }
    }
}

