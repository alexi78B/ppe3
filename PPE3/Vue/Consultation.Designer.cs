﻿namespace PPE3.Vue
{
    partial class Consultation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView1 = new System.Windows.Forms.ListView();
            this.NumDéclaration = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Commentaire = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Date = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Urgence = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Traite = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Problème = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.NumContainer = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.NumDéclaration,
            this.Commentaire,
            this.Date,
            this.Urgence,
            this.Traite,
            this.Problème,
            this.NumContainer});
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(12, 12);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(1041, 373);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.DoubleClick += new System.EventHandler(this.ListView1_DoubleClick);
            // 
            // NumDéclaration
            // 
            this.NumDéclaration.Text = "NumDéclaration";
            this.NumDéclaration.Width = 114;
            // 
            // Commentaire
            // 
            this.Commentaire.Text = "Commentaire";
            this.Commentaire.Width = 128;
            // 
            // Date
            // 
            this.Date.Text = "Date";
            this.Date.Width = 82;
            // 
            // Urgence
            // 
            this.Urgence.Text = "Urgence";
            this.Urgence.Width = 106;
            // 
            // Traite
            // 
            this.Traite.Text = "Traite";
            this.Traite.Width = 63;
            // 
            // Problème
            // 
            this.Problème.Text = "Problème";
            this.Problème.Width = 128;
            // 
            // NumContainer
            // 
            this.NumContainer.Text = "NumContainer";
            this.NumContainer.Width = 112;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(450, 472);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(152, 61);
            this.button1.TabIndex = 1;
            this.button1.Text = "Retour";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // Consultation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1065, 602);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.listView1);
            this.Name = "Consultation";
            this.Text = "Consultation";
            this.Load += new System.EventHandler(this.Consultation_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader NumDéclaration;
        private System.Windows.Forms.ColumnHeader Commentaire;
        private System.Windows.Forms.ColumnHeader Date;
        private System.Windows.Forms.ColumnHeader Urgence;
        private System.Windows.Forms.ColumnHeader Traite;
        private System.Windows.Forms.ColumnHeader Problème;
        private System.Windows.Forms.ColumnHeader NumContainer;
        private System.Windows.Forms.Button button1;
    }
}