﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PPE3.Vue
{
    public partial class MenuDeclaration : Form
    {
        private string _idDocker;
        public MenuDeclaration(string codeDocker)
        {
            InitializeComponent();
            _idDocker = codeDocker;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CreerDeclaration creerDeclaration = new CreerDeclaration(_idDocker);
            creerDeclaration.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ConsultationDeclaration consulterDeclaration = new ConsultationDeclaration(_idDocker);
            consulterDeclaration.Show();
            this.Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
