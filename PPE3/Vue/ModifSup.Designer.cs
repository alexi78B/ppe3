﻿namespace PPE3.Vue
{
    partial class ModifSup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.Traite = new System.Windows.Forms.ComboBox();
            this.Urgent = new System.Windows.Forms.ComboBox();
            this.Probleme = new System.Windows.Forms.ComboBox();
            this.Commentaire = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.CodeDeclaration = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.NumContainer = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(150, 319);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 54);
            this.button1.TabIndex = 0;
            this.button1.Text = "Sauvegarder";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Sauvegarder_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(365, 319);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(120, 54);
            this.button2.TabIndex = 1;
            this.button2.Text = "Supprimer";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Supprimer_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(255, 399);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(120, 54);
            this.button3.TabIndex = 2;
            this.button3.Text = "Retour";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.RetourMenu_Click);
            // 
            // Traite
            // 
            this.Traite.FormattingEnabled = true;
            this.Traite.Location = new System.Drawing.Point(365, 218);
            this.Traite.Name = "Traite";
            this.Traite.Size = new System.Drawing.Size(121, 21);
            this.Traite.TabIndex = 5;
            // 
            // Urgent
            // 
            this.Urgent.FormattingEnabled = true;
            this.Urgent.Location = new System.Drawing.Point(365, 162);
            this.Urgent.Name = "Urgent";
            this.Urgent.Size = new System.Drawing.Size(121, 21);
            this.Urgent.TabIndex = 6;
            // 
            // Probleme
            // 
            this.Probleme.FormattingEnabled = true;
            this.Probleme.Location = new System.Drawing.Point(365, 105);
            this.Probleme.Name = "Probleme";
            this.Probleme.Size = new System.Drawing.Size(121, 21);
            this.Probleme.TabIndex = 7;
            // 
            // Commentaire
            // 
            this.Commentaire.Location = new System.Drawing.Point(12, 105);
            this.Commentaire.Multiline = true;
            this.Commentaire.Name = "Commentaire";
            this.Commentaire.Size = new System.Drawing.Size(258, 134);
            this.Commentaire.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(520, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Problème";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(520, 170);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Urgent";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(520, 226);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Traité";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(98, 73);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Commentaire";
            // 
            // CodeDeclaration
            // 
            this.CodeDeclaration.Location = new System.Drawing.Point(150, 28);
            this.CodeDeclaration.Name = "CodeDeclaration";
            this.CodeDeclaration.Size = new System.Drawing.Size(120, 20);
            this.CodeDeclaration.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(167, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "CodeDeclaration";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(388, 12);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "NumContainer";
            // 
            // NumContainer
            // 
            this.NumContainer.Location = new System.Drawing.Point(365, 28);
            this.NumContainer.Name = "NumContainer";
            this.NumContainer.Size = new System.Drawing.Size(120, 20);
            this.NumContainer.TabIndex = 17;
            // 
            // ModifSup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(647, 506);
            this.Controls.Add(this.NumContainer);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CodeDeclaration);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Commentaire);
            this.Controls.Add(this.Probleme);
            this.Controls.Add(this.Urgent);
            this.Controls.Add(this.Traite);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "ModifSup";
            this.Text = "ModifSup";
            this.Load += new System.EventHandler(this.ModifSup_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ComboBox Traite;
        private System.Windows.Forms.ComboBox Urgent;
        private System.Windows.Forms.ComboBox Probleme;
        private System.Windows.Forms.TextBox Commentaire;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox CodeDeclaration;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox NumContainer;
    }
}