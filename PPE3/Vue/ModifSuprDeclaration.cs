﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PPE3.Vue
{
    public partial class ModifSuprDeclaration : Form
    {
        public ModifSuprDeclaration()
        {
            InitializeComponent();
        }

        private void Sauvegarder_Click(object sender, EventArgs e)
        {
            Consultation consultation = new Consultation();
            consultation.Show();
            this.Hide();
        }
        private void Supprimer_Click(object sender, EventArgs e)
        {
            Consultation consultation = new Consultation();
            consultation.Show();
            this.Hide();
        }
        private void Retour_Click(object sender, EventArgs e)
        {
            Consultation consultation = new Consultation();
            consultation.Show();
            this.Hide();
        }
    }
}
