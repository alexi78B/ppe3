﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using PPE3.Container;

namespace PPE3.Container
{
    public class Declaration
    {
        #region  Modèle Objet

        private long codeDeclaration;
        private long numContainer;
        private string codeProbleme;
        private string commentaireDeclaration;
        private DateTime dateDeclaration;
        private string urgence;
        private string traite;
        private string _idDocker;

        public long CodeDeclaration
        {
            get
            {
                return codeDeclaration;
            }
            set
            {
                codeDeclaration = value;
            }
        }

        public long NumContainer
        {
            get
            {
                return numContainer;
            }
            set
            {
                numContainer = value;
            }
        }

        public string CodeProbleme
        {
            get
            {
                return codeProbleme;
            }
            set
            {
                codeProbleme = value;
            }
        }

        public string CommentaireDeclaration
        {
            get
            {
                return commentaireDeclaration;
            }
            set
            {
                commentaireDeclaration = value;
            }
        }

        public DateTime DateDeclaration
        {
            get
            {
                return dateDeclaration;
            }
            set
            {
                dateDeclaration = value;
            }
        }

        public string Urgence
        {
            get
            {
                return urgence;
            }
            set
            {
                urgence = value;
            }
        }

        public string Traite
        {
            get
            {
                return traite;
            }
            set
            {
                traite = value;
            }
        }

        public Declaration()
        {
            codeDeclaration = -1;
        }

        public string CodeDocker
        {
            get
            {
                return _idDocker;
            }
            set
            {
                _idDocker = value;
            }
        }



        public Declaration(long numContainer, string codeProbleme, string commentaireDeclaration, DateTime dateDeclaration, string urgence, string traite)
        {
            this.numContainer = numContainer;
            this.CodeProbleme = codeProbleme;
            this.commentaireDeclaration = commentaireDeclaration;
            this.dateDeclaration = dateDeclaration;
            this.urgence = urgence;
            this.traite = traite;
        }
        #endregion

        #region Requètes SQL
        private static string _selectSql =
            "SELECT codeDeclaration, numContainer, probleme, commentaireDeclaration, dateDeclaration, urgence, traite FROM DECLARATION where codeDocker=?codeDocker";

        private static string _selectSqlByContainer =
            "SELECT codeDeclaration, d.numContainer, probleme, commentaireDeclaration, dateDeclaration, urgence, traite FROM DECLARATION d, CONTAINER c where typeContainer=?typeContainer AND d.numContainer = c.numContainer";

        private static string _insertSql =
            "INSERT INTO DECLARATION (numContainer, probleme, commentaireDeclaration, dateDeclaration, urgence, traite, codeDocker) VALUES (?numContainer, ?probleme, ?commentaireDeclaration, ?dateDeclaration, ?urgence, ?traite, ?codeDocker)";

        private static string _updateSql =
            "UPDATE DECLARATION SET numContainer=?numContainer, probleme=?probleme, dateDeclaration=?dateDeclaration, urgence=?urgence, traite=?traite, commentaireDeclaration=?commentaireDeclaration WHERE CodeDeclaration=?codeDeclaration";

        private static string _getLastInsertId =
            "SELECT codeDeclaration FROM DECLARATION where numContainer=?numContainer";

        private static string _supprimerSql =
            "delete from DECLARATION where codeDeclaration = ?codeDeclaration";

        #endregion

        #region Modèle de données
        /// <summary>
        /// Retourne une collection de toutes les déclarations
        /// </summary>
        /// <returns></returns>
        public static List<Declaration> FetchAll(string codeDocker)
        {
            List<Declaration> collectionDeclaration = new List<Declaration>();
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _selectSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?codeDocker",codeDocker));
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Declaration uneDeclaration = new Declaration();
                uneDeclaration.codeDeclaration = Convert.ToInt16(jeuEnregistrements["codeDeclaration"].ToString());
                uneDeclaration.numContainer = Convert.ToInt16(jeuEnregistrements["numContainer"].ToString());
                uneDeclaration.codeProbleme = (jeuEnregistrements["probleme"].ToString());
                uneDeclaration.commentaireDeclaration = jeuEnregistrements["commentaireDeclaration"].ToString();
                uneDeclaration.dateDeclaration = Convert.ToDateTime(jeuEnregistrements["dateDeclaration"].ToString());
                uneDeclaration.urgence = jeuEnregistrements["urgence"].ToString();
                uneDeclaration.traite = jeuEnregistrements["traite"].ToString();
                uneDeclaration._idDocker = codeDocker;
                collectionDeclaration.Add(uneDeclaration);
            }
            msc.Close();
            return collectionDeclaration;
        }
        /// <summary>
        /// Retourne une colection de déclaration pour le container mis en parametre
        /// </summary>
        /// <param name="typecontainer"></param>
        /// <returns></returns>
        public static List<Declaration> FetchAllByContainer(string typecontainer)
        {
            List<Declaration> collectionDeclaration = new List<Declaration>();
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _selectSqlByContainer;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?typeContainer", typecontainer));
            commandSql.Prepare();
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Declaration uneDeclaration = new Declaration();
                uneDeclaration.codeDeclaration = Convert.ToInt16(jeuEnregistrements["codeDeclaration"].ToString());
                uneDeclaration.numContainer = Convert.ToInt16(jeuEnregistrements["numContainer"].ToString());
                uneDeclaration.codeProbleme = (jeuEnregistrements["codeProbleme"].ToString());
                uneDeclaration.commentaireDeclaration = jeuEnregistrements["commentaireDeclaration"].ToString();
                uneDeclaration.dateDeclaration = Convert.ToDateTime(jeuEnregistrements["dateDeclaration"].ToString());
                uneDeclaration.urgence = jeuEnregistrements["urgence"].ToString();
                uneDeclaration.traite = jeuEnregistrements["traite"].ToString();
                collectionDeclaration.Add(uneDeclaration);
            }
            msc.Close();
            return collectionDeclaration;
        }


        /// <summary>
        /// Insère une Déclaration dans la base de données
        /// </summary>
        public void Insert()
        {
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _insertSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?numContainer", numContainer));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?probleme", codeProbleme));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?commentaireDeclaration", commentaireDeclaration));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?dateDeclaration", dateDeclaration));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?urgence", urgence));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?traite", traite));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?codeDocker", _idDocker));
            commandSql.Prepare();
            int nbLignesAffectees = commandSql.ExecuteNonQuery();
            codeDeclaration = commandSql.LastInsertedId;
            msc.Close();
        }


        /// <summary>
        /// Supprime une Déclaration dans la base de données
        /// </summary>
        public void Supprimer()
        {
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _supprimerSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?codeDeclaration", CodeDeclaration));
            commandSql.Prepare();
            int nbLignesAffectees = commandSql.ExecuteNonQuery();
            msc.Close();
        }


        /// <summary>
        /// Modifie une Déclaration dans la base de données
        /// </summary>
        public void Sauvegarder()
        {
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _updateSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?codeDeclaration", CodeDeclaration));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?numContainer", NumContainer));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?probleme", CodeProbleme));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?commentaireDeclaration", CommentaireDeclaration));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?dateDeclaration", DateDeclaration));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?urgence", Urgence));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?traite", Traite));
            commandSql.Prepare();
            int nbLignesAffectees = commandSql.ExecuteNonQuery();
            codeDeclaration = commandSql.LastInsertedId;
            msc.Close();
        }

        #endregion

    }
}
