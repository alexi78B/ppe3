﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using PPE3.Container;

namespace PPE3.Container
{
    class DataBaseAccess
    {
        private static string connectionString = "Database=mydb_abes;Data Source=srv-mydon.sio.local;User Id=abes;Password=22/12/1998"; // connexion Mydon
        //private static string connectionString = "Database=mydb_abes;Data Source=sts.ferry-conflans.net;User Id=abes;Password=22/12/1998"; // connexion chez moi

        public static MySqlConnection GetConnexion
        {
            get
            {
                return new MySqlConnection(connectionString);
            }

        }

        public static MySqlParameter CodeParam(string paramName, object value)
        {
            MySqlCommand commandSql = new MySqlCommand();
            MySqlParameter parametre = commandSql.CreateParameter();
            parametre.ParameterName = paramName;
            parametre.Value = value;
            return parametre;
        }

    }
}