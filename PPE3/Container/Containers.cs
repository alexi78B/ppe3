﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using PPE3.Container;

namespace PPE3.Container
{
    public class Containers
    {
        #region Modèle Objet
        private long numContainer;
        private DateTime dateAchat;
        private string typeContainer;
        private DateTime dateLimiteProInsp;
        //  private List<Inspection> lesInspections;

        public long NumContainer
        {
            get
            {
                return numContainer;
            }
            set
            {
                numContainer = value;
            }
        }

        public string TypeContainer
        {
            get
            {
                return typeContainer;
            }
            set
            {
                typeContainer = value;
            }
        }

        public DateTime DateAchat
        {
            get
            {
                return dateAchat;
            }
            set
            {
                dateAchat = value;
            }
        }

        public DateTime DateLimiteProInsp
        {
            get
            {
                return dateLimiteProInsp;
            }
            set
            {
                dateLimiteProInsp = value;
            }
        }

        public Containers()
        {
            numContainer = -1;
        }

        #endregion

        #region Requetes SQL

        private static string _selectSql =  "SELECT * FROM CONTAINER";

        private static string _selectSqlTypeContainer = "Select numContainer from CONTAINER";

        #endregion

        #region Modèle de données

        /// <summary>
        /// Retourne une collection contenant tous les containers
        /// </summary>
        /// <returns></returns>
        public static List<Containers> FetchAll()
        {
            List<Containers> resultat = new List<Containers>();
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _selectSql;
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Containers unContainer = new Containers();

                string numContainer = jeuEnregistrements["numContainer"].ToString();
                unContainer.numContainer = Convert.ToInt16(numContainer);
                unContainer.typeContainer = jeuEnregistrements["typeContainer"].ToString();
                unContainer.DateAchat = Convert.ToDateTime(jeuEnregistrements["dateAchat"].ToString());
                unContainer.DateLimiteProInsp = Convert.ToDateTime(jeuEnregistrements["dateDerniereInsp"]);
                resultat.Add(unContainer);
            }
            msc.Close();
            return resultat;
        }


        /// <summary>
        /// Retourne les numéros de containers
        /// </summary>
        /// <returns></returns>
        public static List<int> AfficheNumContainer()
        {
            List<int> numeroContainer = new List<int>();
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _selectSqlTypeContainer;
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while(jeuEnregistrements.Read())
            {
                numeroContainer.Add(Convert.ToInt16(jeuEnregistrements["numContainer"].ToString()));
            }
            return numeroContainer;
        }
        
        #endregion
    }
}


